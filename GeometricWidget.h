///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	GeometricWidget.h
//	------------------------
//	
//	The main widget that shows the geometry
//	
///////////////////////////////////////////////////

#ifndef _GEOMETRIC_WIDGET_H
#define _GEOMETRIC_WIDGET_H

#include <QGLWidget>
#include <QMouseEvent>
#include <vector>
#include "Cartesian3.h"

class GeometricWidget : public QGLWidget										
	{ // class GeometricWidget
	Q_OBJECT
    public:
    struct GridPosition
    {
        unsigned int squareIndexX, squareIndexY;
        Cartesian3 interpolation;
    };

    std::vector<Cartesian3> vertices;
    std::vector<GridPosition> gridPositions;
    std::vector<std::vector<Cartesian3>> deformationGrid;

	// constructor
    GeometricWidget(QWidget *parent);
	
	// destructor
	~GeometricWidget();

protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);

    private:    
    void initializeSquareGridPositions();
    void initializeTriangularGridPositions();

    void getClosestGridPoint(float coordinateX, float coordinateY, int& outIndexX, int& outIndexY);
    void editGridWithWeights(float moveTowardsX, float moveTowardsY);

    void renderSquareGrid();
    void renderTriangularGrid();

    void updateSquareGrid();
    void updateTriangularGrid();

	}; // class GeometricWidget

#endif
