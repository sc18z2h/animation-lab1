///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	main.cpp
//	------------------------
//	
///////////////////////////////////////////////////

#include <QApplication>
#include "GeometricWidget.h"
#include <stdio.h>

int main(int argc, char **argv)
	{ // main()
	// initialize QT
	QApplication app(argc, argv);

    GeometricWidget aWindow(NULL);

    // 	set the initial size
    aWindow.resize(600, 600);

    // show the window
    aWindow.show();

    // set QT running
    return app.exec();
	} // main()
