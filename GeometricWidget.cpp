///////////////////////////////////////////////////
//
//	Hamish Carr
//	January, 2018
//
//	------------------------
//	GeometricWidget.h
//	------------------------
//	
//	The main widget that shows the geometry
//	
///////////////////////////////////////////////////

#include <math.h>

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <QScreen>
#include <limits>

#include "GeometricWidget.h"
#include "Cartesian3.h"

using namespace std;

const unsigned int gridDimension = 4;

Cartesian3 Barycentric(Cartesian3& p, Cartesian3& a, Cartesian3& b, Cartesian3& c);

// constructor
GeometricWidget::GeometricWidget(QWidget *parent)
    : QGLWidget(parent)
{
    vertices =
    {
        Cartesian3(0, 0.5, 0),
        Cartesian3(-0.5, -0.5, 0),
        Cartesian3(0.5, -0.5, 0),
    };

    deformationGrid.resize(gridDimension);
    for(unsigned int y = 0; y < gridDimension; ++y)
    {
        deformationGrid[y].resize(gridDimension);
        for(unsigned int x = 0; x < gridDimension; ++x)
        {
            deformationGrid[y][x] = Cartesian3(
                (-1 + 2*x/(float)(gridDimension-1))*0.9f,
                (-1 + 2*y/(float)(gridDimension-1))*0.9f,
                0);
        }
    }

    //initializeSquareGridPositions();
    //updateSquareGrid();
    initializeTriangularGridPositions();
    updateTriangularGrid();
} // constructor

// destructor
GeometricWidget::~GeometricWidget()
{ // destructor
    // nothing yet
} // destructor

// called when OpenGL context is set up
void GeometricWidget::initializeGL()
{ // GeometricWidget::initializeGL()
    // enable Z-buffering
    glEnable(GL_DEPTH_TEST);

    // set lighting parameters
    glShadeModel(GL_FLAT);

    // background is pink
    glClearColor(1.0, 1.0, 1.0, 1.0);
} // GeometricWidget::initializeGL()

// called every time the widget is resized
void GeometricWidget::resizeGL(int w, int h)
{ // GeometricWidget::resizeGL()
    // reset the viewport
    glViewport(0, 0, w, h);

    // set projection matrix to be glOrtho based on zoom & window size
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // compute the aspect ratio of the widget
    float aspectRatio = (float) w / (float) h;

    const float size = 1.0;
    glOrtho(-aspectRatio * size, aspectRatio * size, -size, size, -size, size);
} // GeometricWidget::resizeGL()

// called every time the widget needs painting
void GeometricWidget::paintGL()
{ // GeometricWidget::paintGL()
    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set model view matrix based on stored translation, rotation &c.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // render triangle
    glColor3f(0.9f, 0.0f, 0.0f);
    glBegin(GL_TRIANGLES);
    // we will loop in 3's, assuming CCW order
    for (unsigned int vertex = 0; vertex < vertices.size(); )
    { // per triangle
        // use increment to step through them
        Cartesian3 *v0 = &(vertices[vertex++]);
        Cartesian3 *v1 = &(vertices[vertex++]);
        Cartesian3 *v2 = &(vertices[vertex++]);
        // now compute the normal vector
        Cartesian3 uVec = *v1 - *v0;
        Cartesian3 vVec = *v2 - *v0;
        Cartesian3 normal = uVec.cross(vVec).normalise();

        glNormal3fv(&normal.x);
        glVertex3fv(&v0->x);
        glVertex3fv(&v1->x);
        glVertex3fv(&v2->x);
    } // per triangle
    glEnd();

    // draw grid lines
    //renderSquareGrid();
    renderTriangularGrid();
} // GeometricWidget::paintGL()

#include <iostream>
// mouse-handling
void GeometricWidget::mousePressEvent(QMouseEvent *event)
{ // GeometricWidget::mousePressEvent()
    // store the button for future reference
    Qt::MouseButton whichButton = event->button();
    Cartesian3 mouseWorldPosition(
        -1.0f + 2.0f * event->x() / (float) width(),
        +1.0f - 2.0f * event->y() / (float) height(),
        0);

    // now either translate or rotate object or light
    switch(whichButton)
    { // button switch
    case Qt::RightButton:
        vertices[0] = mouseWorldPosition;
        updateGL();
        break;
    case Qt::MiddleButton:
        // update the widget
        vertices[1] = mouseWorldPosition;
        updateGL();
        break;
    case Qt::LeftButton:
        // update the widget
        vertices[2] = mouseWorldPosition;
        updateGL();
        break;
    case Qt::ForwardButton:
    {
        int indexX = -1;
        int indexY = -1;

//        getClosestGridPoint(mouseWorldPosition.x, mouseWorldPosition.y, indexX, indexY);
//        if(indexX != -1 && indexY != -1)
//        {
//            deformationGrid[indexY][indexX] = mouseWorldPosition;
//            //updateSquareGrid();
//            updateTriangularGrid();
//        }

        editGridWithWeights(mouseWorldPosition.x, mouseWorldPosition.y);
        //updateSquareGrid();
        updateTriangularGrid();

        updateGL();
        break;
    }
    default:
        break;
    } // button switch
} // GeometricWidget::mousePressEvent()

void GeometricWidget::mouseMoveEvent(QMouseEvent *event)
{ // GeometricWidget::mouseMoveEvent()
    // store the button for future reference
    Qt::MouseButton whichButton = event->button();

    // now either translate or rotate object or light
    switch(whichButton)
    { // button switch
    case Qt::RightButton:
        // update the widget
        updateGL();
        break;
    case Qt::MiddleButton:
        // update the widget
        updateGL();
        break;
    case Qt::LeftButton:
        // update the widget
        updateGL();
        break;
    default:
        break;
    } // button switch
} // GeometricWidget::mouseMoveEvent()

void GeometricWidget::mouseReleaseEvent(QMouseEvent *event)
{ // GeometricWidget::mouseReleaseEvent()
    // store the button for future reference
    Qt::MouseButton whichButton = event->button();

    // now either translate or rotate object or light
    switch(whichButton)
    { // button switch
    case Qt::RightButton:
        // just update
        updateGL();
        break;
    case Qt::MiddleButton:
        // update the widget
        updateGL();
        break;
    case Qt::LeftButton:
        // update the widget
        updateGL();
        break;
    default:
        break;
    } // button switch
} // GeometricWidget::mouseReleaseEvent()

void GeometricWidget::initializeSquareGridPositions()
{
    gridPositions.resize(vertices.size());
    for(unsigned int i = 0; i < vertices.size(); ++i)
    {
        gridPositions[i] = {(unsigned int)-1,(unsigned int)-1,Cartesian3()};
        for(unsigned int y = 0; y + 1 < gridDimension && gridPositions[i].squareIndexX == (unsigned int)-1; ++y)
        {
            for(unsigned int x = 0; x + 1 < gridDimension; ++x)
            {
                if(deformationGrid[y][x].x <= vertices[i].x &&
                    deformationGrid[y][x].y <= vertices[i].y &&
                    vertices[i].x < deformationGrid[y + 1][x + 1].x &&
                    vertices[i].y < deformationGrid[y + 1][x + 1].y)
                {
                    float rangeX = deformationGrid[y + 1][x + 1].x - deformationGrid[y][x].x;
                    float rangeY = deformationGrid[y + 1][x + 1].y - deformationGrid[y][x].y;
                    float offsetX = vertices[i].x - deformationGrid[y][x].x;
                    float offsetY = vertices[i].y - deformationGrid[y][x].y;
                    gridPositions[i] = {x,y,Cartesian3(offsetX/rangeX, offsetY/rangeY, 0)};
                    break;
                }
            }
        }
    }
}

void GeometricWidget::initializeTriangularGridPositions()
{
    gridPositions.resize(vertices.size());
    for(unsigned int i = 0; i < vertices.size(); ++i)
    {
        gridPositions[i] = {(unsigned int)-1,(unsigned int)-1,Cartesian3()};
        for(unsigned int y = 0; y + 1 < gridDimension && gridPositions[i].squareIndexX == (unsigned int)-1; ++y)
        {
            for(unsigned int x = 0; x + 1 < gridDimension; ++x)
            {
                if(deformationGrid[y][x].x <= vertices[i].x &&
                    deformationGrid[y][x].y <= vertices[i].y &&
                    vertices[i].x < deformationGrid[y + 1][x + 1].x &&
                    vertices[i].y < deformationGrid[y + 1][x + 1].y)
                {
                    float offsetX = vertices[i].x - deformationGrid[y][x].x;
                    float offsetY = vertices[i].y - deformationGrid[y][x].y;

                    // store triangle indices in pairs, odd = bottom right, even = top left
                    unsigned int gridIndexX = x * 2;
                    Cartesian3 trianglePointB;
                    if(offsetY < offsetX)
                    {
                        //bottom right triangle
                        ++gridIndexX;
                        trianglePointB = deformationGrid[y][x+1];
                    }
                    else
                    {
                        //top left triangle
                        trianglePointB = deformationGrid[y+1][x];
                    }

                    Cartesian3 barycentricCoordinates = Barycentric(vertices[i], deformationGrid[y][x], trianglePointB, deformationGrid[y+1][x+1]);
                    gridPositions[i] = {gridIndexX,y,barycentricCoordinates};
                    break;
                }
            }
        }
    }
}

void GeometricWidget::getClosestGridPoint(float coordinateX, float coordinateY, int& outIndexX, int& outIndexY)
{
    outIndexX = outIndexY = -1;
    float closestDistanceSquared = std::numeric_limits<float>::infinity();

    for (unsigned int y = 0; y < deformationGrid.size(); ++y)
    {
        for (unsigned int x = 0; x < deformationGrid[y].size(); ++x)
        {
            float differenceX = coordinateX - deformationGrid[y][x].x;
            float differenceY = coordinateY - deformationGrid[y][x].y;
            float currentDistanceSquared = differenceX * differenceX + differenceY * differenceY;

            if(currentDistanceSquared < closestDistanceSquared)
            {
                outIndexX = x;
                outIndexY = y;
                closestDistanceSquared = currentDistanceSquared;
            }
        }
    }
}

void GeometricWidget::editGridWithWeights(float moveTowardsX, float moveTowardsY)
{
    const float weight = 0.005f;

    for (unsigned int y = 0; y < deformationGrid.size(); ++y)
    {
        for (unsigned int x = 0; x < deformationGrid[y].size(); ++x)
        {
            Cartesian3 vectorTowards = Cartesian3(moveTowardsX, moveTowardsY, 0) - deformationGrid[y][x];
            float distanceAway = vectorTowards.length();
            vectorTowards = vectorTowards / distanceAway;
            float moveDistance = weight / distanceAway;

            if(distanceAway <= moveDistance)
                deformationGrid[y][x] = Cartesian3(moveTowardsX, moveTowardsY, 0);
            else
                deformationGrid[y][x] = deformationGrid[y][x] + vectorTowards * moveDistance;
       }
    }
}

void GeometricWidget::renderSquareGrid()
{
    glColor3f(0.1f, 0.1f, 0.8f);
    glBegin(GL_LINES);

    for (unsigned int y = 0; y < deformationGrid.size(); ++y)
    {
        for (unsigned int x = 0; x + 1 < deformationGrid[y].size(); ++x)
        {
            glVertex3f(deformationGrid[y][x].x, deformationGrid[y][x].y, 0.1);
            glVertex3f(deformationGrid[y][x+1].x, deformationGrid[y][x+1].y, 0.1);
        }
    }

    for (unsigned int y = 0; y + 1 < deformationGrid.size(); ++y)
    {
        for (unsigned int x = 0; x < deformationGrid[y].size(); ++x)
        {
            glVertex3f(deformationGrid[y][x].x, deformationGrid[y][x].y, 0.1);
            glVertex3f(deformationGrid[y+1][x].x, deformationGrid[y+1][x].y, 0.1);
        }
    }

    glEnd();
}

void GeometricWidget::renderTriangularGrid()
{
    renderSquareGrid();

    glColor3f(0.1f, 0.1f, 0.8f);
    glBegin(GL_LINES);

    for (unsigned int y = 0; y + 1 < deformationGrid.size(); ++y)
    {
        for (unsigned int x = 0; x + 1 < deformationGrid[y].size(); ++x)
        {
            glVertex3f(deformationGrid[y][x].x, deformationGrid[y][x].y, 0.1);
            glVertex3f(deformationGrid[y+1][x+1].x, deformationGrid[y+1][x+1].y, 0.1);
        }
    }

    glEnd();
}



void GeometricWidget::updateSquareGrid()
{
    for(unsigned int i = 0; i < gridPositions.size(); ++i)
    {
        unsigned int x = gridPositions[i].squareIndexX;
        unsigned int y = gridPositions[i].squareIndexY;

        if(x == (unsigned int)-1 || y == (unsigned int)-1)
            continue;

        Cartesian3 interpolateTop =
                deformationGrid[y][x] * (1 - gridPositions[i].interpolation.x) +
                deformationGrid[y][x+1] * gridPositions[i].interpolation.x;
        Cartesian3 interpolateBottom =
                deformationGrid[y+1][x] * (1 - gridPositions[i].interpolation.x) +
                deformationGrid[y+1][x+1] * gridPositions[i].interpolation.x;
        Cartesian3 interpolateMiddle =
                interpolateTop * (1 - gridPositions[i].interpolation.y) +
                interpolateBottom * gridPositions[i].interpolation.y;

        vertices[i] = interpolateMiddle;
    }
}

void GeometricWidget::updateTriangularGrid()
{
    for(unsigned int i = 0; i < gridPositions.size(); ++i)
    {
        unsigned int x = gridPositions[i].squareIndexX;
        unsigned int y = gridPositions[i].squareIndexY;

        if(x == (unsigned int)-1 || y == (unsigned int)-1)
            continue;

        unsigned int gridIndexX = x / 2;
        Cartesian3 trianglePointB;
        if(x % 2 == 0)
        {
            // top left triangle
            trianglePointB = deformationGrid[y+1][gridIndexX];
        }
        else
        {
            // bottom right triangle
            trianglePointB = deformationGrid[y][gridIndexX+1];
        }

        vertices[i] =
            deformationGrid[y][gridIndexX] * gridPositions[i].interpolation.x +
            trianglePointB * gridPositions[i].interpolation.y +
            deformationGrid[y+1][gridIndexX+1] * gridPositions[i].interpolation.z;
    }
}

// Compute barycentric coordinates (u, v, w) for
// point p with respect to triangle (a, b, c)
Cartesian3 Barycentric(Cartesian3& p, Cartesian3& a, Cartesian3& b, Cartesian3& c)
{
    Cartesian3 v0 = b - a, v1 = c - a, v2 = p - a;
    float d00 = v0.dot(v0);
    float d01 = v0.dot(v1);
    float d11 = v1.dot(v1);
    float d20 = v2.dot(v0);
    float d21 = v2.dot(v1);
    float denom = d00 * d11 - d01 * d01;
    float v = (d11 * d20 - d01 * d21) / denom;
    float w = (d00 * d21 - d01 * d20) / denom;
    float u = 1.0f - v - w;
    return Cartesian3(u,v,w);
}
